package projeto.trilha.repository;

import org.springframework.data.repository.CrudRepository;

import projeto.trilha.model.Trilha;

public interface TrilhaRepository extends CrudRepository<Trilha, Integer> {
}
