package projeto.trilha.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import projeto.trilha.model.Palestra;

public interface PalestraRepository extends CrudRepository<Palestra, Integer> {
	
	public List<Palestra> findByTrilhaId(Integer id);
	
	public List<Palestra> findByPalestrante(String palestrante);
}
