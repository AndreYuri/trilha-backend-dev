package projeto.trilha.repository;

import org.springframework.data.repository.CrudRepository;

import projeto.trilha.model.Palestrante;

public interface PalestranteRepository extends CrudRepository<Palestrante, Integer> {


}
