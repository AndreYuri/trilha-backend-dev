package projeto.trilha.repository;

import org.springframework.data.repository.CrudRepository;

import projeto.trilha.model.User;

public interface UserRepository extends CrudRepository<User, Integer> {
}
