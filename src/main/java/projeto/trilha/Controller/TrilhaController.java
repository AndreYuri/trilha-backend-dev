package projeto.trilha.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import projeto.trilha.model.Trilha;
import projeto.trilha.repository.TrilhaRepository;

import java.util.Optional;

@RestController
@RequestMapping("/trilha")
public class TrilhaController {

	@Autowired
	private TrilhaRepository repository;

	@RequestMapping(value = "/buscar/{id}", method = RequestMethod.GET)
	public ResponseEntity<Trilha> get(@PathVariable Integer id) {
		Optional<Trilha> trilhaQueVeioDoBancoDeDados = repository.findById(id);

		return new ResponseEntity<Trilha>(trilhaQueVeioDoBancoDeDados.get(), HttpStatus.OK);
	}

	@RequestMapping(value = "/deletar/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Trilha> delete(@PathVariable Integer id) {

		repository.deleteById(id);

		return new ResponseEntity<Trilha>(new Trilha(), HttpStatus.OK);
	}

	@RequestMapping(value = "/inserir", method = RequestMethod.POST)
	public ResponseEntity<Trilha> post(@RequestBody Trilha trilha) {

		repository.save(trilha);

		return new ResponseEntity<Trilha>(trilha, HttpStatus.OK);
	}

	@RequestMapping(value = "/atualizar/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Trilha> put(@RequestBody Trilha trilha) {

		repository.save(trilha);

		return new ResponseEntity<Trilha>(trilha, HttpStatus.OK);
	}
}
