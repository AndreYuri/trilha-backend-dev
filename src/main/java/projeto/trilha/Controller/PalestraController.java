package projeto.trilha.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import projeto.trilha.model.Palestra;
import projeto.trilha.repository.PalestraRepository;

@RestController
@RequestMapping("/palestra")
public class PalestraController {

	@Autowired
	private PalestraRepository repository;

	@RequestMapping(value = "/buscar/{id}", method = RequestMethod.GET)
	public ResponseEntity<Palestra> get(@PathVariable Integer id) {

		Optional<Palestra> palestraQueVeioDoBancoDeDados = repository.findById(id);

		return new ResponseEntity<Palestra>(palestraQueVeioDoBancoDeDados.get(), HttpStatus.OK);
	}

	@RequestMapping(value = "/deletar/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Palestra> delete(@PathVariable Integer id) {

		repository.deleteById(id);

		return null;
	}

	@RequestMapping(value = "/inserir", method = RequestMethod.POST)
	public ResponseEntity<Palestra> post(@RequestBody Palestra palestra) {

		repository.save(palestra);

		return new ResponseEntity<Palestra>(palestra, HttpStatus.OK);
	}

	@RequestMapping(value = "/atualizar/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Palestra> put(@RequestBody Palestra palestra) {

		repository.save(palestra);

		return new ResponseEntity<Palestra>(palestra, HttpStatus.OK);
	}

	@RequestMapping(value = "/buscarportrilha/{id}", method = RequestMethod.GET)
	public ResponseEntity<List<Palestra>> buscarPorTrilha(@PathVariable Integer id) {

		List<Palestra> palestras = repository.findByTrilhaId(id);

		return new ResponseEntity<List<Palestra>>(palestras, HttpStatus.OK);
	}

	@RequestMapping(value = "/buscarpornome/{palestrante}", method = RequestMethod.GET)
	public ResponseEntity<List<Palestra>> buscarPorNome(@PathVariable String palestrante) {

		List<Palestra> palestrantes = repository.findByPalestrante(palestrante);

		return new ResponseEntity<List<Palestra>>(palestrantes, HttpStatus.OK);
	}
}