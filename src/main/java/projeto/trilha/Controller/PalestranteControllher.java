package projeto.trilha.controller;	

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import projeto.trilha.model.Palestrante;
import projeto.trilha.repository.PalestranteRepository;

@RestController
@RequestMapping("/palestrante")
public class PalestranteControllher {

	@Autowired
	private PalestranteRepository repository;

	@RequestMapping(value = "/buscar/{id}", method = RequestMethod.GET)
	public ResponseEntity<Palestrante> get(@PathVariable Integer id) {

		Optional<Palestrante> palestrantesVindoDoBanco = repository.findById(id);

		return new ResponseEntity<Palestrante>(palestrantesVindoDoBanco.get(), HttpStatus.OK);

	}

	@RequestMapping(value = "/deletar/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Palestrante> delete(@PathVariable Integer id) {

		repository.deleteById(id);

		return null;
	}

	@RequestMapping(value = "/inserir", method = RequestMethod.POST)
	public ResponseEntity<Palestrante> post(@RequestBody Palestrante palestrante) {

		repository.save(palestrante);

		return new ResponseEntity<Palestrante>(palestrante, HttpStatus.OK);
	}

	@RequestMapping(value = "/atualizar/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Palestrante> updatePalestrante(@RequestBody Palestrante palestrante,
			@PathVariable Integer id) {

		repository.save(palestrante);

		return new ResponseEntity<Palestrante>(palestrante, HttpStatus.OK);
	}

}
