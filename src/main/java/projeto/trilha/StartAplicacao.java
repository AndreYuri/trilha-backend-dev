package projeto.trilha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StartAplicacao {
    public static void main( String[] args ){
        SpringApplication.run(StartAplicacao.class, args);
    }
}
