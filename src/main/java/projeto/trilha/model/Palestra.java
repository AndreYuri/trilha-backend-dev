package projeto.trilha.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Palestra {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private String titulo;

	private String descricao;

	private String palestrante;

	private String nome_palestrante;

	private String data;

	@OneToOne
	private Trilha trilha;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getPalestrante() {
		return palestrante;
	}

	public void setPalestrante(String palestrante) {
		this.palestrante = palestrante;
	}

	public String getNome_palestrante() {
		return nome_palestrante;
	}

	public void setNome_palestrante(String nome_palestrante) {
		this.nome_palestrante = nome_palestrante;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public Trilha getTrilha() {
		return trilha;
	}

	public void setTrilha(Trilha trilha) {
		this.trilha = trilha;
	}

}
