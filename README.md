# Documentação Backend

## Teste

Deverá ser passado em Postman dois parametros na Hearders!
- KEY: Content-Type 
- VALUE: Aplication/Json

## URL´s das API´s

###  `trilha/buscar/{id}`
url utilizada para buscar os dados de uma entidade utilizando como parametro o Identificador da Entidade.
retorna um HTTPSTATUS 200.

###  `trilha/deletar/{id}`
url utilizada para deletar uma entidade utilizando como parametro o Identificador da Entidade.
retorna um HTTPSTATUS 200.

###  `trilha/inserir`
url utilizada para inserir os dados de uma entidade. segue abaixo o exemplo do JSON:
retorna um HTTPSTATUS 200.

{  "palestra" : "JAVA",
    "sala" : "01",
    "nome" : "André Yuri",
    "data" : "02/07/2019"
}

###  `trilha/atualizar`
url utilizada para inserir os dados de uma entidade. segue abaixo o exemplo do JSON:
retorna um HTTPSTATUS 200.

{  "palestra" : "valor_aqui",
    "sala" : "valor_aqui",
    "nome" : "valor_aqui",
    "data" : "valor_aqui"
}

### ###

### `palestra/buscar/{id}`
url utilizada para buscar os dados de uma entidade utilizando como parametro o Identificador da Entidade.
retorna um HTTPSTATUS 200.

###  `palestra/deletar/{id}`
url utilizada para deletar uma entidade utilizando como parametro o Identificador da Entidade.
retorna um HTTPSTATUS 200.

###  `palestra/inserir`
url utilizada para inserir os dados de uma entidade. segue abaixo o exemplo do JSON:
retorna um HTTPSTATUS 200.

{  "titulo" : "MicroServiços",
    "descricao" : "Micro Serviços mostrando suas vantagens",
    "palestrante" : "Aaron Gillespie",
    "data" : "02/07/2019"
    "trilha" : {
    	"id" : 10
    }
}

###  `palestra/atualizar`
url utilizada para inserir os dados de uma entidade. segue abaixo o exemplo do JSON:
retorna um HTTPSTATUS 200.

{  "titulo" : "valor_aqui",
    "descricao" : "valor_aqui",
    "palestrante" : "valor_aqui",
    "Data" : "valor_aqui"
}

### ###

### `palestrante/buscar/{id}`
url utilizada para buscar os dados de uma entidade utilizando como parametro o Identificador da Entidade.
retorna um HTTPSTATUS 200.

###  `palestrante/deletar/{id}`
url utilizada para deletar uma entidade utilizando como parametro o Identificador da Entidade.
retorna um HTTPSTATUS 200

###  `palestrante/inserir`
url utilizada para inserir os dados de uma entidade. segue abaixo o exemplo do JSON:
retorna um HTTPSTATUS 200.

{  "nome" : "André Yuri",
    "empresa" : "Outspoken",
    "email" : "a@outspoken.com"
}

###  `palestrante/atualizar`
url utilizada para inserir os dados de uma entidade. segue abaixo o exemplo do JSON:
retorna um HTTPSTATUS 200.

{  "nome" : "valor_aqui",
    "empresa" : "valor_aqui",
    "email" : "valor_aqui"
}